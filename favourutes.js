let coun = 0;
let summ = 0;
let cartt = {};

if (localStorage.getItem("coun")) {
    count = parseInt(localStorage.getItem("coun"));
}

if (localStorage.getItem("summ")) {
    sum = parseInt(localStorage.getItem("summ"));
}

if (localStorage.getItem("heartt")) {
    cart = JSON.parse(localStorage.getItem("heartt"));
}

updateCart();

let btnss = document.querySelectorAll(".heartt");

for (let i = 0; i < btnss.length; i++) {
    let btn = btnss[i];
    btn.addEventListener("click", add);
}

function add(event) {
    let price = Number(event.target.dataset.price);
    let title = event.target.dataset.title;
    let id = event.target.dataset.id;

if (id in cartt) {
    cartt[id].qty++;
} else {
    let carttItem = {
        title: title,
        price: price,
        qty: 1
    };
    cartt[id] = carttItem
}

    coun++;
    summ += price;

    console.log(cartt);

    localStorage.setItem("heartt", JSON.stringify(cartt));
    updateCart();
}

function updateCart() {
    document.getElementById("sum").textContent = sum;
    document.getElementById("count").textContent = count;
    localStorage.setItem("sum", sum);
    localStorage.setItem("count", count);
}